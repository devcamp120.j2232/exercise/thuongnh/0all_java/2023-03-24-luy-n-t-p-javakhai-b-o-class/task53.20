import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        // khởi tạo hai đối tượng hình chữ nhật ..
        // hình chữ nhật rectangle1 
        Rectangle rectangle1 = new Rectangle();
        // hình chữ nhật rectangle2
        Rectangle rectangle2 = new Rectangle(5.0f  , 10.0f);

        // in ra  màn hình các giấ trị của phương thức 
        // hình chữ nhật rectangle1 
        System.out.println("hình chữ nhật rectangle1");
        System.out.println(rectangle1.getLength());
        System.out.println(rectangle1.getWidth());
        rectangle1.setLength(5.6f);
        rectangle1.setWidth(1.0f);
        System.out.println(rectangle1.getArena());
        System.out.println(rectangle1.getPerimeter());
        System.out.println(rectangle1.toString());

        // hình chữ nhật rectangle2
        System.out.println("hình chữ nhật rectangle2");
        System.out.println(rectangle2.getLength());

        System.out.println(rectangle2.getWidth());
        // rectangle2.setLength(5.6f);
        // rectangle2.setWidth(1.0f);
        System.out.println(rectangle2.getArena());
        System.out.println(rectangle2.getPerimeter());
        System.out.println(rectangle2.toString());
        



    }
}
